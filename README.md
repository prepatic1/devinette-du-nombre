# Fichier readme du TP
## créer un nouveau projet sur Gitlab nommé << Devinette d'un nombre >> et le cloner sur votre ordinateur
![Q1](capture d'ecran/clonnage.png)
## créer dans le dossier cloné un fichier << main.py>> et écrire le programme du jeu
### première écriture du programme
![Q2.1](capture d'ecran/écriture1.png)
### enrégistrement 
![Q2.2](capture d'ecran/enrégistrement.png)
## propagation sur un dépot distant
### distribution vers le dossier distant 
![Q3](capture d'ecran/distribution.png)
## création d'une nouvelle branche
![Q4](capture d'ecran/création et switch.png)
## dévelloper dans cette branche de nouvelle modification
### réecriture du programme avec modification 
![Q5](capture d'ecran/éceiture 2.png)
### distribution
![Q5](capture d'ecran/distributio2.png)
## Fusion
![Q6](capture d'ecran/Fusion.png)

